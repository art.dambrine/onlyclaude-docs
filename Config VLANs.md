# Configuration des VLANs via Unifi

## 1. Création des VLANs sous forme de "réseau entreprise"

![](images/conf_vlan_20.png)

**NOTE** : on créé un "réseau usage entreprise"

- On donne une range DHCP pour profiter pour le moment du DHCP interne au routeur sur nos VLANs.
- Communication du DNS interne de notre entreprise "Serveur de Nom DHCP".

**Exemple de configuration finale du réseau VLANs :**

![](images/conf_vlan_fin.png)

## 2. Ajustement de la config des ports sur le switch Unifi

Nous configurons en mode "All" (= trunk) les ports 15 et 16 qui sont respectivement connectés au routeur et à l'ESXi

Les autres ports sont répartis entre VLANs Non cadre et Cadre.

![](images/config_port_switch.png)



## 3 .Configuration des VLANs virtuels ESXi

Ne pas oublier de créer des configurations de VLANs dans ESXi afin d'attribuer nos machines virtuelles à tel ou tel VLAN.

![](images\esxi_creation_cartes_reseau.png)



<img src="images/esxi_id_vlan_carte.png" style="zoom:80%;" />